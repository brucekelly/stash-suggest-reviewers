package com.atlassian.bitbucket.server.suggestreviewers.internal.rest;

import com.atlassian.bitbucket.NoSuchEntityException;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.i18n.I18nService;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.rest.BadRequestException;
import com.atlassian.bitbucket.rest.util.ResponseFactory;
import com.atlassian.bitbucket.rest.util.RestUtils;
import com.atlassian.bitbucket.server.suggestreviewers.PullRequestDetails;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewerService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Path("by")
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Singleton
public class SuggestedReviewersResource {

    private final SuggestedReviewerService suggestedReviewerService;
    private final CommitService commitService;
    private final RefService refService;
    private final RepositoryService repositoryService;
    private final I18nService i18nService;

    public SuggestedReviewersResource(SuggestedReviewerService suggestedReviewerService, CommitService commitService,
                                      RefService refService, RepositoryService repositoryService,
                                      I18nService i18nService) {
        this.suggestedReviewerService = suggestedReviewerService;
        this.commitService = commitService;
        this.refService = refService;
        this.repositoryService = repositoryService;
        this.i18nService = i18nService;
    }

    @GET
    @Path("sha")
    public Response forShas(@QueryParam("fromRepoId") Integer fromRepoId,
                            @QueryParam("from") String fromSha,
                            @QueryParam("toRepoId") Integer toRepoId,
                            @QueryParam("to") String toSha,
                            @QueryParam("count") @DefaultValue("5") int count) {
        checkParams(fromRepoId, fromSha, toRepoId, toSha);

        // not found exceptions automatically handled by ExceptionMapper

        Commit toHead = commitService.getCommit(new CommitRequest.Builder(requireRepository(toRepoId), toSha).build());
        Commit fromHead = commitService.getCommit(new CommitRequest.Builder(requireRepository(fromRepoId), fromSha).build());

        return suggestReviewers(new PullRequestDetails.Builder().fromCommit(fromHead).toCommit(toHead).build(), count);
    }

    @GET
    @Path("ref")
    public Response forRefs(@QueryParam("fromRepoId") Integer fromRepoId,
                            @QueryParam("from") String fromRef,
                            @QueryParam("toRepoId") Integer toRepoId,
                            @QueryParam("to") String toRef,
                            @QueryParam("count") @DefaultValue("5") int count) {
        checkParams(fromRepoId, fromRef, toRepoId, toRef);

        // not found exceptions automatically handled by ExceptionMapper
        Commit toHead = getLatestCommit(requireRepository(toRepoId), toRef);
        Commit fromHead = getLatestCommit(requireRepository(fromRepoId), fromRef);

        PullRequestDetails details = new PullRequestDetails.Builder()
                .fromCommit(fromHead)
                .toCommit(toHead)
                .fromRefId(fromRef)
                .toRefId(toRef)
                .build();
        return suggestReviewers(details, count);
    }

    private void checkParams(Integer fromRepoId, String from, Integer toRepoId, String to) {
        if (fromRepoId == null || from == null || toRepoId == null || to == null) {
            throw new BadRequestException(i18nService.getText("suggest.reviewers.params.required",
                    "The 'fromRepoId', 'from', 'toRepoId' and 'to' parameters are all required."));
        }
    }

    private Repository requireRepository(int fromRepoId) {
        Repository repository = repositoryService.getById(fromRepoId);
        if (repository == null) {
            KeyedMessage message = i18nService.getKeyedText("suggest.reviewer.no.such.repository", "No repository with id {0} exists.", fromRepoId);
            throw new NoSuchRepositoryException(message, null);
        }
        return repository;
    }

    private Response suggestReviewers(PullRequestDetails details, int count) {
        Iterable<SuggestedReviewer> reviewers = suggestedReviewerService.getSuggestedReviewers(details, count);
        Iterable<RestSuggestedReviewer> restReviewers = Lists.newArrayList(Iterables.transform(reviewers, RestSuggestedReviewer.TO_REST));
        return ResponseFactory.ok(restReviewers).build();
    }

    private Commit getLatestCommit(Repository repository, String refName) {
        Ref ref = refService.resolveRef(repository, refName);
        if (ref == null) {
            throw new NoSuchEntityException(i18nService.getKeyedText("no.such.ref", "Couldn't find ref: {0}", refName));
        }
        return commitService.getCommit(new CommitRequest.Builder(repository, ref.getLatestCommit()).build());
    }

}
