package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git;

import com.atlassian.bitbucket.io.LineReader;
import com.atlassian.bitbucket.io.LineReaderOutputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.fugue.Pair;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiffNumstatOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Map<String, Pair<Integer, Integer>>> {

    private static final Pattern NUMSTAT = Pattern.compile("^(\\d+)\\s+(\\d+)\\s+(.*)$");

    private final Map<String, Pair<Integer, Integer>> pathDeltas = new HashMap<>();

    public DiffNumstatOutputHandler() {
        super("UTF-8");
    }

    @Nullable
    @Override
    public Map<String, Pair<Integer, Integer>> getOutput() {
        return pathDeltas;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            Matcher m = NUMSTAT.matcher(line);
            if (m.matches()) {
                pathDeltas.put(m.group(3), Pair.pair(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))));
            }
        }
    }

}
