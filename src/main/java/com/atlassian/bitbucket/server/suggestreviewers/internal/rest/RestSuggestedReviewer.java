package com.atlassian.bitbucket.server.suggestreviewers.internal.rest;

import com.atlassian.bitbucket.rest.RestMapEntity;
import com.atlassian.bitbucket.rest.user.RestApplicationUser;
import com.atlassian.bitbucket.server.suggestreviewers.SuggestedReviewer;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RestSuggestedReviewer extends RestMapEntity {

    public static Function<SuggestedReviewer, RestSuggestedReviewer> TO_REST = new Function<SuggestedReviewer, RestSuggestedReviewer>() {
        @Override
        public RestSuggestedReviewer apply(SuggestedReviewer input) {
            return new RestSuggestedReviewer(input);
        }
    };

    public RestSuggestedReviewer(SuggestedReviewer suggestedReviewer) {
        put("user", new RestApplicationUser(suggestedReviewer.getUser()));
        put("shortReason", suggestedReviewer.getShortReason());
        put("reasons", Lists.newArrayList(suggestedReviewer.getReasons()));
    }

}
