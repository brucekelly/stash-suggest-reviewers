package com.atlassian.bitbucket.server.suggestreviewers.internal.suggester.git;

import com.atlassian.bitbucket.io.LineReader;
import com.atlassian.bitbucket.io.LineReaderOutputHandler;
import com.atlassian.bitbucket.scm.CommandOutputHandler;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiffSummaryOutputHandler extends LineReaderOutputHandler implements CommandOutputHandler<Void>, DiffSummary {

    private static final Pattern SUMMARY = Pattern.compile("^\\s(delete|create)\\smode\\s(\\d{6})\\s(.*)$");

    private final Set<String> deletedPaths = new HashSet<>();
    private final Set<String> createdPaths = new HashSet<>();

    public DiffSummaryOutputHandler() {
        super("UTF-8");
    }

    @Nullable
    @Override
    public Void getOutput() {
        return null;
    }

    public Set<String> getCreatedPaths() {
        return createdPaths;
    }

    public Set<String> getDeletedPaths() {
        return deletedPaths;
    }

    @Override
    protected void processReader(LineReader lineReader) throws IOException {
        String line;
        while ((line = lineReader.readLine()) != null) {
            Matcher m = SUMMARY.matcher(line);
            if (m.matches()) {
                String status = m.group(1);
                String path = m.group(3);
                if ("create".matches(status)) {
                    createdPaths.add(path);
                } else if ("delete".matches(status)) {
                    deletedPaths.add(path);
                }
            }
        }
    }

}
