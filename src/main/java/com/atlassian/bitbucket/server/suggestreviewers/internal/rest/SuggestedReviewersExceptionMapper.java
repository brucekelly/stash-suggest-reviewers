package com.atlassian.bitbucket.server.suggestreviewers.internal.rest;

import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapper;
import com.atlassian.bitbucket.rest.exception.UnhandledExceptionMapperHelper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class SuggestedReviewersExceptionMapper extends UnhandledExceptionMapper {

    public SuggestedReviewersExceptionMapper(UnhandledExceptionMapperHelper helper) {
        super(helper);
    }

}
